<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/','ListController@index');

Route::auth();



Route::get('/add', 'UserController@addForm');
Route::post('/add', 'UserController@add');
Route::get('/dashboard', 'UserController@dashboard');
Route::get('/delete/{id}', 'UserController@delete');
Route::get('/city/{id}', 'UserController@cityList');
Route::get('/model/{id}', 'UserController@modelList');
Route::get('/cityFilter/{id}/{cityValue}', 'ListController@cityFilter');
Route::get('/modelFilter/{id}/{modelValue}', 'ListController@modelFilter');