<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests;
use App\Ad;
use App\Area;
use App\Brand;
use App\Car;
use App\City;
use App\Mod;
use App\Place;
use App\User;

class ListController extends Controller
{
    protected $post_per_page = 10 ;
    public function index(Request $request){
        $messages = array(
            'area.integer' => 'Фильтр Область должен быть выбран в выпадающем списке',
            'city.integer' => 'Фильтр Город должно быть выбран в выпадающем списке',
            'brand.integer' => 'Фильтр Марка должно быть выбран в выпадающем списке',
            'model.integer' => 'Фильтр Модель должно быть выбран в выпадающем списке',
            'capacity.regex' => 'Фильтр Объём Двигателя должен соответсвовать формату: число.число',
            'mileage.integer' => 'Фильтр Пробег должен быть целым числом',
            'owner_count.integer' => 'Фильтр Колличество Владельцев должен быть целым числом',
            'price.integer' => 'Фильтр Цена должен быть целым числом',
        );
        $v = Validator::make($request->all(),[
            'area' => 'integer',
            'city' => 'integer',
            'brand' => 'integer',
            'model' => 'integer',
            'capacity' => 'regex:/^[+-]?\d+\.\d+/',
            'mileage' => 'integer',
            'owner_count' => 'integer',
            'price' => 'integer',
        ],$messages);
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        $placeArray = array();
        $carArray = array();
        $userArray = array();
        $areasName = array();
        $citiesName = array();
        $brandName = array();
        $modelName = array();
        $userName = array();
        $PlacesId = array();
        $CarsId = array();
        $photoArray = array();
        if( $request->area != 0 || $request->city != 0 || $request->brand != 0 || $request->model != 0 || $request->capacity != NULL || $request->mileage != NULL || $request->owner_count != NULL || $request->price != NULL  ){
             if($request->area!=0){
                 $findArea = Place::all()->where('area_id',intval($request->area));
                 foreach ($findArea as $Areas) {
                     $PlacesId[] = $Areas->id;
                 }
                 $query = Ad::whereIn('places_id',$PlacesId);
             }
            if($request->area!=0 && $request->city != 0 ){
                $findPlace = Place::all()->where('area_id',intval($request->area))->where('city_id',intval($request->city));
                foreach ($findPlace as $Place) {
                    $PlaceId = $Place->id;
                }
                $query = Ad::where('places_id',$PlaceId);
            }
            if($request->brand != 0){
                $findBrand = Car::all()->where('brand_id',intval($request->brand));
                foreach ($findBrand as $Brands) {
                    $CarsId[] = $Brands->id;
                }
                if($request->area == 0){
                    $query = Ad::whereIn('cars_id',$CarsId);
                }
                else{
                    $query = $query->whereIn('cars_id',$CarsId);
                }

            }
            if($request->brand != 0 && $request->model != 0){
                $findCar = Car::all()->where('brand_id',intval($request->brand))->where('model_id',intval($request->model));
                foreach ($findCar as $Car) {
                    $CarId = $Car->id;
                }
                if($request->area == 0){
                    $query = Ad::where('cars_id',$CarId);
                }
                else{
                    $query = $query->where('cars_id',$CarId);
                }

            }
            if($request->capacity != NULL){
                if($request->area == 0 && $request->brand == 0){
                    $query = Ad::where('capacity','<',$request->capacity);
                  //  var_dump($query);
                }
                else{
                    $query = $query->where('capacity','<',$request->capacity);
                }
            }
            if($request->mileage != NULL){
                if($request->area == 0 && $request->brand == 0 && $request->capacity == NULL){
                    $query = Ad::where('mileage','<',intval($request->mileage));
                    //  var_dump($query);
                }
                else{
                    $query = $query->where('mileage','<',intval($request->mileage));
                }
            }
            if($request->owner_count != NULL){
                if($request->area == 0 && $request->brand == 0 && $request->capacity == NULL && $request->mileage == NULL){
                    $query = Ad::where('owner_count','<',intval($request->owner_count));
                    //  var_dump($query);
                }
                else{
                    $query = $query->where('owner_count','<',intval($request->owner_count));
                }
            }
            if($request->price != NULL){
                if($request->area == 0 && $request->brand == 0 && $request->capacity == NULL && $request->mileage == NULL && $request->owner_count == NULL){
                    $query = Ad::where('price','<',intval($request->price));
                    //  var_dump($query);
                }
                else{
                    $query = $query->where('price','<',intval($request->price));
                }
            }

            $shows = $query->orderBy('id', 'DESC')->paginate($this->post_per_page);
       }
        else {
            $shows = Ad::orderBy('id', 'DESC')->paginate($this->post_per_page);
        }
         foreach ($shows as $show){
             $placeArray[] = $show->places_id;
             $carArray[] = $show->cars_id;
             $userArray[] = $show->user_id;
             $photoArray[$show->id] = Storage::files($show->id);
         }
         for($i = 0; $i <count($placeArray);$i++){
             $placeInfo = Place::find($placeArray[$i]);
             $areasId = $placeInfo->area_id;
             $areasInfo = Area::find($areasId);
             $cityId = $placeInfo->city_id;
             $cityInfo = City::find($cityId);
             $citiesName[$placeArray[$i]] = $cityInfo->name;
             $areasName[$placeArray[$i]] = $areasInfo->name;
         }
         for($i = 0; $i <count($carArray);$i++){
             $carInfo = Car::find($carArray[$i]);
             $brandId = $carInfo->brand_id;
             $brandInfo = Brand::find($brandId);
             $modelId = $carInfo->model_id;
             $modelInfo = Mod::find($modelId);
             $brandName[$carArray[$i]] = $brandInfo->name;
             $modelName[$carArray[$i]] = $modelInfo->name;
         }
         for($i = 0; $i <count($userArray);$i++){
            $userInfo = User::find($userArray[$i]);
             $userName[$userArray[$i]] =  $userInfo->name;
         }
        /* var_dump($areasName);
         var_dump($citisName);
         var_dump($brandName);
         var_dump($modelName);
         var_dump($userName);*/
        $data['areas'] = Area::all();
        $data['cities'] = City::all();
        $data['brands'] = Brand::all();
        $data['models'] = Mod::all();
        $data['shows'] = $shows;
        $data['areasName'] = $areasName;
        $data['citiesName'] = $citiesName;
        $data['brandName'] = $brandName;
        $data['modelName'] = $modelName;
        $data['userName'] = $userName;
        $data['photoArray'] = $photoArray;
        //$files = Storage::files('27');
        //print_r($photoArray);
      if( $request->ajax()){
          return view('ajax.index')->with(compact('data'));
        }
        return view('index')->with(compact('data'));
    }
    public function cityFilter($id,$cityValue = 0){
        $cityInArray = array();
        $placeLists = Place::all()->where('area_id',intval($id));
        foreach ($placeLists as $placeList){
            $cityInArray[] = $placeList->city_id;
        }
        $cityList = City::all()->whereIn('id',$cityInArray);
        $data['cityList'] = $cityList;
        $data['cityValue'] = $cityValue;
        if($id == 0 ){
            echo  '<option value="0">Выберете область чтобы выбрать город</option>';
        }
        else {
            return view('cityFilter')->with('data', $data);
        }
    }
    public function modelFilter($id,$modelValue = 0){
        $modelInArray = array();
        $carLists = Car::all()->where('brand_id',intval($id));
        foreach ($carLists as $carList){
            $modelInArray[] = $carList->model_id;
        }
        $modelList = Mod::all()->whereIn('id',$modelInArray);
        $data['modelList'] = $modelList;
        $data['modelValue'] = $modelValue;
        if($id == 0 ){
            echo  '<option value="0">Выберете марку чтобы выбрать модель</option>';
        }
        else {
            return view('modelFilter')->with('data', $data);
        }

    }

}
