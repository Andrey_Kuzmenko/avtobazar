<?php

namespace App\Http\Controllers;
use App\Ad;
use App\Area;
use App\Brand;
use App\Car;
use App\City;
use App\Mod;
use App\Place;
use App\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function dashboard()
    {
        $carInfo = array();
        $userInfo = User::find(Auth::id());
        $ads = Ad::all()->where('user_id',Auth::id());
         foreach ($ads as $ad){
             $cars = Car::find($ad->cars_id);
             $model = Mod::find($cars->model_id);
             $brand = Brand::find($cars->brand_id);
             $carInfo[$ad->cars_id][0] = $brand;
             $carInfo[$ad->cars_id][1] = $model;
         }
         $data['userInfo'] = $userInfo;
         $data['ads'] = $ads;
         $data['carInfo'] = $carInfo;
         return view('dashboard')->with('data',$data);
    }
    public function delete($id)
    {
        $ads = Ad::find($id);
        $ads->delete();
        Storage::deleteDirectory($id);
        return redirect('dashboard');
    }

    public function addForm()
    {
        $areasArray = Area::all();
        $citiesArray = City::all();
        $modelArray = Mod::all();
        $brandArray = Brand::all();
        $data['areasArray'] = $areasArray;
        $data['citiesArray'] = $citiesArray;
        $data['modelArray'] = $modelArray;
        $data['brandArray'] = $brandArray;
        return view('add')->with('data',$data);;
    }

    public function add(Request $request)
    {
        $messages = array( 'area.required' => 'Поле Область должно быть заполненно',
            'area.integer' => 'Поле Область должно быть выбрано в выпадающем списке',
            'city.required' => 'Поле Город должно быть заполненно',
            'city.integer' => 'Поле Город должно быть выбрано в выпадающем списке',
            'brand.required' => 'Поле Марка должно быть заполненно',
            'brand.integer' => 'Поле Марка должно быть выбрано в выпадающем списке',
            'model.required' => 'Поле Модель должно быть заполненно',
            'model.integer' => 'Поле Модель должно быть выбрано в выпадающем списке',
            'capacity.required' => 'Поле Объём Двигателя должно быть заполненно',
            'capacity.regex' => 'Поле Объём Двигателя должно соответсвовать формату: число.число',
            'mileage.required' => 'Поле Пробег должно  быть заполненно',
            'mileage.integer' => 'Поле Пробег должно быть целым числом',
            'owner_count.integer' => 'Поле Колличество Владельцев должно быть целым числом',
            'owner_count.required' => 'Поле Колличество Владельцев должно  быть заполненно',
            'price.required' => 'Поле Цена должно  быть заполненно',
            'price.integer' => 'Поле Цена должно быть целым числом',
            );
        $v = Validator::make($request->all(),[
            'user_id' => 'required|integer',
            'area' => 'required|integer',
            'city' => 'required|integer',
            'brand' => 'required|integer',
            'model' => 'required|integer',
            'capacity' => 'required|regex:/^[+-]?\d+\.\d+/',
            'mileage' => 'required|integer',
            'owner_count' => 'required|integer',
            'price' => 'required|integer',
        ],$messages);
        if ($v->fails())
        {
            return redirect()->back()->withErrors($v->errors());
        }
        $count = Ad::all()->where('user_id',Auth::id())->count();
        /*var_dump($request->all());*/
        $places = Place::all()->where('area_id',intval($request->area))->where('city_id',intval($request->city));
        foreach ($places as $place){
            $place_id = $place->id;
        }
        $cars = Car::all()->where('brand_id',intval($request->brand))->where('model_id',intval($request->model));
        foreach ($cars as $car){
            $car_id = $car->id;
        }
        if($count < 3) {

              $ads = new Ad ;
              $ads->user_id = Auth::id();
              $ads->places_id = $place_id;
              $ads->cars_id = $car_id;
              $ads->capacity = $request->capacity;
              $ads->mileage = $request->mileage;
              $ads->owner_count = $request->owner_count;
              //$ads->photo = $path;
              $ads->price = $request->price;
              $ads->save();
             if($request->file('file') != NULL){

                 $adFinds = Ad::all()->where('user_id',Auth::id());
                foreach ($adFinds as $adFind){
                    $adId = $adFind->id;
                }
                $files = $request->file('file');
                 //var_dump(storage_path());
                 $extensions = array("jpg","jpeg","png");
                 $i = 0;
                 if(!empty($files)){
                     foreach ($files as $file){
                         if(in_array($file->getclientoriginalextension(),$extensions)) {
                             Storage::put("$adId/" . $file->getClientOriginalName(), file_get_contents($file));
                             $i++;
                         }
                     }
                 }
                 if($i != 0) {
                     $path = '/storage/app/'.$adId;
                     $updateAdd = Ad::find($adId);
                     $updateAdd->photo = $path;
                     $updateAdd->save();
                 }
                 else{
                     Storage::deleteDirectory($adId);
                     return redirect()->back()->withErrors('Фото неправильного разрешения! Фото должно иметь разрешение: JPG,JPEG,PNG');
                 }
             }
              return redirect()->back()->with('success', 'Объявление добаленно!');
          }
          else{
              return redirect()->back()->withErrors('У вас есть уже 3 объявления!');
          }

    }
    public function cityList($id){
        $cityInArray = array();
        $placeLists = Place::all()->where('area_id',intval($id));
        foreach ($placeLists as $placeList){
            $cityInArray[] = $placeList->city_id;
        }
        $cityList = City::all()->whereIn('id',$cityInArray);
        if($id == 0){
            echo "<option>Выберете область чтобы выбрать город</option>";
        }
        else {
            return view('cityList')->with('cityList', $cityList);
        }
    }
    public function modelList($id){
        $modelInArray = array();
        $carLists = Car::all()->where('brand_id',intval($id));
        foreach ($carLists as $carList){
            $modelInArray[] = $carList->model_id;
        }
        $modelList = Mod::all()->whereIn('id',$modelInArray);
        if($id == 0){
            echo " <option>Выберете марку чтобы выбрать модель</option>";
        }
        else {
            return view('modelList')->with('modelList', $modelList);
        }
    }

}
