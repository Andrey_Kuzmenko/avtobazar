<option value="0">Модель не выбрана</option>
@foreach($data['modelList'] as $model )
    @if($model->id == $data['modelValue'])
        <option value="{{$model->id}}" selected>{{$model->name}}</option>
    @else
        <option value="{{$model->id}}">{{$model->name}}</option>
    @endif

@endforeach