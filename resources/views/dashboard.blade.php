@extends('layouts.app')
<?php $key = 1 ?>
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="panel panel-default">
                    <div class="panel-heading">Панель управления {{$data['userInfo']->name}}</div>

                    <div class="panel-body">
                        Мои Объявления <br>
                        максимальное колличество объявлений -  3
                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Машина</th>
                                <th>Цена</th>
                                <th> </th>
                            </tr>
                            </thead>
                            <tbody>

                            @foreach($data['ads'] as $ad )
                                <tr>
                                    <td>{{$key++}}</td>
                                    <td>{{$data['carInfo'][$ad->cars_id][0]->name}} {{$data['carInfo'][$ad->cars_id][1]->name}}</td>
                                    <td>{{$ad->price}} $</td>
                                    <td><a href="delete/{{$ad->id}}" class="btn btn-danger">Удалить</a></td>
                                </tr>
                            @endforeach
                            @if(count($data['ads']) == 0)
                                <tr>
                                   <td></td>
                                    <td>У вас нет ещё объявлений</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            @endif
                            </tbody>
                        </table>
                        <a href="add" class="btn btn-success" role="button"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Добавить Объявление</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
