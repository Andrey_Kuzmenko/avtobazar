@extends('layouts.app')

@section('content')
    <div class="container">
    @if (count($errors) > 0)
        <!-- Список ошибок формы -->
            <div id="error" class="alert alert-danger">
                <strong>Произошли такие ошибки:</strong>

                <br><br>

                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        @if(session()->has('success'))
            <div id = "success" class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Информация о Объявлении </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="add" enctype="multipart/form-data">
                            {{ csrf_field() }}

                            <div class="form-group" hidden="hidden">
                                <div class="col-md-3">
                                    <input name ="user_id" value="{{\Illuminate\Support\Facades\Auth::id()}}" >
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="area" class="col-md-4 control-label">Область*</label>

                                <div class="col-md-6">
                                    <select id="area" name="area" required>
                                        <option>Выберете область</option>
                                        @foreach($data['areasArray'] as $area )
                                                <option value="{{$area->id}}">{{$area->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label  class="col-md-4 control-label">Город*</label>

                                <div class="col-md-6">
                                    <select id="city" name="city" required>
                                        <option>Выберете область чтобы выбрать город</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="brand" class="col-md-4 control-label">Марка*</label>

                                <div class="col-md-6">
                                    <select id="brand" name="brand" required>
                                        <option>Выберете Марку</option>
                                        @foreach($data['brandArray'] as $brand )
                                              <option value="{{$brand->id}}">{{$brand->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="model" class="col-md-4 control-label">Модель*</label>

                                <div class="col-md-6">
                                    <select id="model" name="model" required>
                                        <option>Выберете марку чтобы выбрать модель</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="capacity" class="col-md-4 control-label">Объём двигателя см3*</label>

                                <div class="col-md-6" >
                                    <input id="capacity" type="text" class="form-control" name="capacity" value="" required>
                                    <h4><span id="error_capacity"class="label label-danger"></span></h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mileage" class="col-md-4 control-label">Пробег км*</label>

                                <div class="col-md-6" >
                                    <input id="mileage" type="text" class="form-control" name="mileage" value="" required>
                                    <h4><span id="error_mileage"class="label label-danger"></span></h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Фотографии объявления</label>

                                <div class="col-md-6" >
                                    <input type="file" name="file[]" multiple><br/>

                                </div>
                                <label class="col-md-8 control-label">Возможные разрешения: jpg, jpeg , png </label>

                            </div>
                            <div class="form-group">
                                <label for="owner_count" class="col-md-4 control-label">Колличество владельцев*</label>

                                <div class="col-md-6" >
                                    <input id="owner_count" type="text" class="form-control" name="owner_count" value="" required>
                                    <h4><span id="error_owner_count"class="label label-danger"></span></h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price" class="col-md-4 control-label">Цена $*</label>

                                <div class="col-md-6" >
                                    <input id="price" type="text" class="form-control" name="price" value="" required>
                                    <h4><span id="error_price"class="label label-danger"></span></h4>
                                </div>
                            </div>
                            <label class="col-md-8 control-label">* Обязательные поля для заполнения</label>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Добавить
                                    </button>
                                    <a class="btn btn-link" href="../dashboard">Назад </a>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function() {
            $('#area').change(function(){
                url = "/public/city/"+$('#area').val();

                $.get(url, function(data){
                    $('#city').html(data);
                });

            });
            $('#brand').change(function(){
                url = "/public/model/"+$('#brand').val();
                $.get(url, function(data){
                    $('#model').html(data);
                });

            });
            setTimeout(function() {
                $("#error").hide(2000);
            }, 5000);
            setTimeout(function() {
                $("#success").hide(2000);
            }, 2000);
            $('#capacity').blur(function () {
                str = $(this).val();
                str = str.replace(",",".");
                regex = /^[+-]?\d+\.$/g;
                if((regex.exec(str)) !== null){
                    str = str + '0';
                    $('#capacity').val(str);
                    }
                regex2 = /^[+-]?\d+\.\d+/;
                if(regex2.exec(str) !== null){
                    $('#capacity').val(str);
                }
                else {
                    if(str !="") {
                        str = str + '.0';
                        $('#capacity').val(str);
                    }
                }
                regexLetter = /([A-Za-zа-яА-Я])/;
                if(regexLetter.exec(str) !== null){
                    $('#capacity').val("");
                    $('#error_capacity').text("Введите число а не буквы");
                    $("#error_capacity").show();
                    error_capacity();
                }

            });
            $('#mileage').blur(function () {
                str = $(this).val();
                regexLetter = /([A-Za-zа-яА-Я])/;
                if(regexLetter.exec(str) !== null){
                    $('#mileage').val("");
                    $('#error_mileage').text("Введите число а не буквы");
                    $("#error_mileage").show();
                    error_mileage();
                }
            });
            $('#owner_count').blur(function () {
                str = $(this).val();
                regexLetter = /([A-Za-zа-яА-Я])/;
                if(regexLetter.exec(str) !== null){
                    $('#owner_count').val("");
                    $('#error_owner_count').text("Введите число а не буквы");
                    $("#error_owner_count").show();
                    error_owner_count();
                }
            });
            $('#price').blur(function () {
                str = $(this).val();
                regexLetter = /([A-Za-zа-яА-Я])/;
                if(regexLetter.exec(str) !== null){
                    $('#price').val("");
                    $('#error_price').text("Введите число а не буквы");
                    $("#error_price").show();
                    error_price();
                }
            });

            setTimeout(function() {
                $("#error").hide(2000);
            }, 5000);

            function error_capacity() {
                setTimeout(function() {
                    $("#error_capacity").hide(500);
                    $('#error_capacity').text();
                }, 2000);
            }
            function error_mileage() {
                setTimeout(function () {
                    $('#error_mileage').hide(500);
                    $('#error_mileage').text();
                }, 2000);
            }
            function error_owner_count() {
                setTimeout(function () {
                    $('#error_owner_count').hide(500);
                    $('#error_owner_count').text();
                }, 2000);
            }
            function error_price() {
                setTimeout(function () {
                    $('#error_price').hide(500);
                    $('#error_price').text();
                }, 2000);
            }
        })

    </script>
@endsection

