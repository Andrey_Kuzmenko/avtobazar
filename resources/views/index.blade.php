@extends('layouts.app')
@section('content')

    <div class="container">
    @if (count($errors) > 0)
        <!-- Список ошибок формы -->
            <div id="error" class="alert alert-danger">
                <strong>Параметры фильтрации заданы неправильно:</strong>
                <br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-4 ">
                <form id="formx">
                <div class="col-md-8">
                    <label for="area" class="col-md-4">Область</label>
                    <select id="area" name="area">
                        <option value="0">Выберете область</option>
                        @foreach($data['areas'] as $area)
                            <option value="{{$area->id}}">{{$area->name}}</option>
                        @endforeach
                    </select>
                </div>
                <br>
                    <div class="col-md-12">
                    <label  for="city" class="col-md-4">Город</label>
                    <select id="city" name="city">
                        <option value="0">Выберете область чтобы выбрать город</option>
                    </select>
                </div>
                <br>
                <div class="col-md-8">
                    <label for="brand" class="col-md-4">Марка</label>
                    <select id="brand" name="brand">
                        <option value="0">Выберете марку</option>
                        @foreach($data['brands'] as $brand)
                            <option value="{{$brand->id}}">{{$brand->name}}</option>
                        @endforeach
                    </select>
                </div>
                    <br>
                    <div class="col-md-12">
                    <label for="model" class="col-md-4">Модель</label>
                    <select id="model" name="model">
                        <option value="0">Выберете марку чтобы выбрать модель</option>
                    </select>
                </div>
                    <br>
                    <div class="col-md-12">
                    <label   class="col-md-4">Объём двигателя менее</label><input id="capacity" name="capacity" placeholder="Введите объём двигателя">
                      <h4><span id="error_capacity"class="label label-danger"></span></h4>

                </div>
                    <br>
                    <div class="col-md-12">
                    <label class="col-md-4">Пробег менее</label><input id="mileage" name="mileage" placeholder="Введите желаемый порог пробега">
                        <h4><span id="error_mileage"class="label label-danger"></span></h4>
                    </div>
                    <div class="col-md-12">
                    <label class="col-md-4">Владельцев менее</label><input id="owner_count" name="owner_count" placeholder="Введите колличество владельцев">
                        <h4><span id="error_owner_count"class="label label-danger"></span></h4>
                </div>
                    <div class="col-md-12">
                    <label class="col-md-4">Цена менее</label><input id="price" name="price" placeholder="Введите цену">
                        <h4><span id="error_price"class="label label-danger"></span></h4>
                </div>

                    <button type="submit">Применить фильтры</button>

                </form>
            </div>
            <section class="posts">
            <div class="col-md-8 ">

            @foreach($data['shows'] as $show)
                    <div class="panel panel-default">
                        <div class="panel-heading"> {{$data['brandName'][$show->cars_id]}} - {{$data['modelName'][$show->cars_id]}}</div>
                        <div class="panel-body">
                            @if($show->photo != NULL)
                                @for($i = 0 ; $i <  count($data['photoArray'][$show->id]); $i++)
                                <p class="col-md-4"><img src="../../../storage/app/{{$data['photoArray'][$show->id][$i]}}"
                                                                    width="185" height="119.5" >
                                </p>
                            @endfor
                            @else
                                <p class="col-md-4"><img src="../../../storage/no-img.png"
                                                                width="185" height="119.5" alt="No image">
                                </p>
                            @endif
                            <label class="col-md-4">{{$data['userName'][$show->user_id]}}</label>
                            <label class="col-md-4">Область: {{$data['areasName'][$show->places_id]}} </label>
                            <label class="col-md-4">Город: {{$data['citiesName'][$show->places_id]}}</label>
                            <label class="col-md-4">Объём двигателя: {{$show->capacity}}</label>
                            <label class="col-md-4">Пробег км: {{$show->mileage}}</label>
                            <label class="col-md-4">Колличество владельцев  {{$show->owner_count}} человек(а)</label>
                            <label class="col-md-4">Цена  {{$show->price}} $</label>
                            <label class="col-md-4">Дата публикации  {{$show->created_at}} </label>
                        </div>
                    </div>
                @endforeach
                @if(count($data['shows']) == 0)
                    <div class="panel panel-default">
                    <div class="panel-heading"> Авотомобилей не найдено</div>
                    <div class="panel-body">
                        <h3>Жаль,но автомобилей с заданными условиями не найдено. Поменяйте условия и попробуйте ещё раз. Спасибо что вы с нами!</h3>
                        </div>
                    </div>
                @endif
                {{$data['shows']->render()}}
            </div>
            </section>
        </div>

    </div>

@endsection
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script>

    $(document).ready(function() {

        var $_GET = {};

        document.location.search.replace(/\??(?:([^=]+)=([^&]*)&?)/g, function () {
            function decode(s) {
                return decodeURIComponent(s.split("+").join(" "));
            }

            $_GET[decode(arguments[1])] = decode(arguments[2]);

        });
            $('body').on('click', '.pagination a', function(e){

         e.preventDefault();
         var url = $(this).attr('href');

         $.get(url, function(data){
         $('.posts').html(data);
         });

         });
     $('#area').change(function(){
         if($_GET['city']!= undefined) {
             url = "/public/cityFilter/" + $('#area').val() + "/" + $_GET['city'];
         }
         else {
             url = "/public/cityFilter/" + $('#area').val()+ "/" +0;
         }
            $.get(url, function(data){
                $('#city').html(data);
            });
     });
        $('#brand').change(function(){
            if($_GET['model']!= undefined) {
                url = "/public/modelFilter/"+$('#brand').val()+ "/"+ $_GET['model'];
            }
            else {
                url = "/public/modelFilter/"+$('#brand').val()+"/"+0;
            }
            $.get(url, function(data){
                $('#model').html(data);
            });

        });

        if($_GET['area']!= undefined) {
            $('#area option[value=' + $_GET['area'] + ']').attr('selected', 'true');
            $('#area').trigger( "change" );
        }
        if($_GET['brand']!= undefined) {
            $('#brand option[value=' + $_GET['brand'] + ']').attr('selected', 'true');
            $('#brand').trigger( "change" );
        }
        $('#capacity').blur(function () {
            str = $(this).val();
            str = str.replace(",",".");
            regex = /^[+-]?\d+\.$/g;
            if((regex.exec(str)) !== null){
                str = str + '0';
                $('#capacity').val(str);
                $('#error_capacity').text("");
            }
            regex2 = /^[+-]?\d+\.\d+/;
            if(regex2.exec(str) !== null){
                $('#capacity').val(str);
                $('#error_capacity').text("");
            }
            else {
                if(str !="") {
                    str = str + '.0';
                    $('#capacity').val(str);
                    $('#error_capacity').text("");
                }
            }
            regexLetter = /([A-Za-zа-яА-Я])/;
            if(regexLetter.exec(str) !== null){
                $('#capacity').val("");
                $('#error_capacity').text("Введите число а не буквы");
                $("#error_capacity").show();
                error_capacity();
            }
        });
        $('#mileage').blur(function () {
            str = $(this).val();
            regexLetter = /([A-Za-zа-яА-Я])/;
            if(regexLetter.exec(str) !== null){
                $('#mileage').val("");
                $('#error_mileage').text("Введите число а не буквы");
                $("#error_mileage").show();
                error_mileage();
            }
        });
        $('#owner_count').blur(function () {
            str = $(this).val();
            regexLetter = /([A-Za-zа-яА-Я])/;
            if(regexLetter.exec(str) !== null){
                $('#owner_count').val("");
                $('#error_owner_count').text("Введите число а не буквы");
                $("#error_owner_count").show();
                error_owner_count();
            }
        });
        $('#price').blur(function () {
            str = $(this).val();
            regexLetter = /([A-Za-zа-яА-Я])/;
            if(regexLetter.exec(str) !== null){
                $('#price').val("");
                $('#error_price').text("Введите число а не буквы");
                $("#error_price").show();
                error_price();
            }
        });

        setTimeout(function() {
            $("#error").hide(2000);
        }, 5000);

        function error_capacity() {
            setTimeout(function() {
            $("#error_capacity").hide(500);
            $('#error_capacity').text();
            }, 2000);
        }
        function error_mileage() {
            setTimeout(function () {
                $('#error_mileage').hide(500);
                $('#error_mileage').text();
            }, 2000);
        }
        function error_owner_count() {
            setTimeout(function () {
                $('#error_owner_count').hide(500);
                $('#error_owner_count').text();
            }, 2000);
        }
        function error_price() {
            setTimeout(function () {
                $('#error_price').hide(500);
                $('#error_price').text();
            }, 2000);
        }
    });
</script>
