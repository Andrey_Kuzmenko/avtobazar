<div class="col-md-8 ">
    @foreach($data['shows'] as $show)
        <div class="panel panel-default">
            <div class="panel-heading"> {{$data['brandName'][$show->cars_id]}} - {{$data['modelName'][$show->cars_id]}}</div>
            <div class="panel-body">
                @if($show->photo != NULL)
                    @for($i = 0 ; $i <  count($data['photoArray'][$show->id]); $i++)
                        <p class="col-md-4"><img src="../../../storage/app/{{$data['photoArray'][$show->id][$i]}}"
                                                            width="185" height="119.5" >
                        </p>
                    @endfor
                @else
                    <p class="col-md-4"><img src="../../../storage/no-img.png"
                                             width="185" height="119.5" alt="No image">
                    </p>
                @endif
                <label class="col-md-4">{{$data['userName'][$show->user_id]}}</label>
                <label class="col-md-4">Область: {{$data['areasName'][$show->places_id]}} </label>
                <label class="col-md-4">Город: {{$data['citiesName'][$show->places_id]}}</label>
                <label class="col-md-4">Объём двигателя: {{$show->capacity}}</label>
                <label class="col-md-4">Пробег км: {{$show->mileage}}</label>
                <label class="col-md-4">Колличество владельцев  {{$show->owner_count}} человек(а)</label>
                <label class="col-md-4">Цена  {{$show->price}} $</label>
                <label class="col-md-4">Дата публикации  {{$show->created_at}} </label>
            </div>
        </div>
    @endforeach
    {{$data['shows']->render()}}
</div>