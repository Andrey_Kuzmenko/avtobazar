@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Информация о Объявлении </div>
                    <div class="panel-body">
                        <form class="form-horizontal" role="form" method="POST" action="">
                            {{ csrf_field() }}

                            <div class="form-group">
                                <label class="col-md-4 control-label">Имя пользователя</label>

                                <div class="col-md-6">
                                <label class="col-md-4 control-label">{{$data['userInfo']->name}}</label>
                                </div>
                            </div>

                            <div class="form-group">
                                <label for="area" class="col-md-4 control-label">Область</label>

                                <div class="col-md-6">
                                    <select id="area" name="area">
                                        @foreach($data['areasArray'] as $area )
                                            @if($area->id == $data['placeInfo'][$data['ad']->places_id][0]->id)
                                             <option selected value="{{$area->id}}">{{$area->name}}</option>
                                           @else
                                            <option value="{{$area->id}}">{{$area->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="city" class="col-md-4 control-label">Город</label>

                                <div class="col-md-6">
                                    <select id="city" name="city">
                                        @foreach($data['citiesArray'] as $city )
                                            @if($city->id == $data['placeInfo'][$data['ad']->places_id][1]->id)
                                            <option selected value="{{$city->id}}">{{$city->name}}</option>
                                            @else
                                            <option value="{{$city->id}}">{{$city->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="brand" class="col-md-4 control-label">Марка</label>

                                <div class="col-md-6">
                                    <select id="brand" name="brand">
                                        @foreach($data['brandArray'] as $brand )
                                            @if($brand->id == $data['carInfo'][$data['ad']->cars_id][0]->id)
                                                <option selected value="{{$brand->id}}">{{$brand->name}}</option>
                                            @else
                                                <option value="{{$brand->id}}">{{$brand->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="model" class="col-md-4 control-label">Модель</label>

                                <div class="col-md-6">
                                    <select id="model" name="model">
                                        @foreach($data['modelArray'] as $model )
                                            @if($model->id == $data['carInfo'][$data['ad']->cars_id][1]->id)
                                                <option selected value="{{$model->id}}">{{$model->name}}</option>
                                            @else
                                                <option value="{{$model->id}}">{{$model->name}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="capacity" class="col-md-4 control-label">Объём двигателя см3</label>

                                <div class="col-md-6" >
                                    <input id="capacity" type="text" class="form-control" name="capacity" value="{{$data['ad']->capacity}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="mileage" class="col-md-4 control-label">Пробег км</label>

                                <div class="col-md-6" >
                                    <input id="mileage" type="text" class="form-control" name="mileage" value="{{$data['ad']->mileage}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Фото</label>

                                <div class="col-md-6" >
                                    <label>Тут может быть фото вашего авто =)</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="owner_count" class="col-md-4 control-label">Колличество владельцев</label>

                                <div class="col-md-6" >
                                    <input id="owner_count" type="text" class="form-control" name="owner_count" value="{{$data['ad']->owner_count}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="price" class="col-md-4 control-label">Цена $</label>

                                <div class="col-md-6" >
                                    <input id="price" type="text" class="form-control" name="price" value="{{$data['ad']->price}}">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-6 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Обновить
                                    </button>
                                    <a class="btn btn-link" href="../dashboard">Назад </a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
