<option value="0">Город не выбран</option>
@foreach($data['cityList'] as $city )
    @if($city->id == $data['cityValue'])
        <option value="{{$city->id}}" selected>{{$city->name}}</option>
    @else
    <option value="{{$city->id}}">{{$city->name}}</option>
    @endif
@endforeach