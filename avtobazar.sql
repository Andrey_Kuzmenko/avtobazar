-- phpMyAdmin SQL Dump
-- version 4.4.15.7
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Апр 24 2017 г., 13:22
-- Версия сервера: 5.5.50
-- Версия PHP: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `avtobazar`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ads`
--

CREATE TABLE IF NOT EXISTS `ads` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `places_id` int(10) NOT NULL,
  `cars_id` int(10) NOT NULL,
  `capacity` float NOT NULL,
  `mileage` int(11) NOT NULL,
  `photo` varchar(200) DEFAULT NULL,
  `owner_count` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `price` int(11) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `ads`
--

INSERT INTO `ads` (`id`, `user_id`, `places_id`, `cars_id`, `capacity`, `mileage`, `photo`, `owner_count`, `created_at`, `updated_at`, `price`) VALUES
(1, 1, 7, 3, 1.6, 25000, '', 1, '2017-04-22 12:48:46', '2017-04-18 13:09:45', 600),
(2, 1, 1, 1, 1.6, 27000, NULL, 2, '2017-04-20 07:05:40', '2017-04-20 07:05:40', 34445),
(6, 1, 10, 2, 1.9, 23000, NULL, 2, '2017-04-21 06:02:21', '2017-04-21 06:02:21', 5000),
(7, 2, 8, 2, 1.6, 27000, 'storage/app/7', 1, '2017-04-22 12:49:08', '2017-04-22 07:44:33', 500),
(28, 2, 2, 1, 1.6, 23, '/storage/app/28', 1, '2017-04-22 13:51:18', '2017-04-22 10:51:18', 5000),
(33, 4, 8, 5, 1.7, 350000, '/storage/app/33', 3, '2017-04-24 08:59:35', '2017-04-24 05:59:35', 600),
(34, 4, 7, 8, 2.5, 46534, '/storage/app/34', 3, '2017-04-24 09:11:28', '2017-04-24 06:11:28', 5000),
(35, 4, 9, 6, 3, 255000, '/storage/app/35', 2, '2017-04-24 09:15:09', '2017-04-24 06:15:09', 800),
(36, 5, 5, 10, 1.3, 27000, '/storage/app/36', 2, '2017-04-24 09:36:04', '2017-04-24 06:36:04', 800),
(37, 5, 4, 7, 2, 27000, '/storage/app/37', 2, '2017-04-24 09:38:59', '2017-04-24 06:38:59', 2000),
(38, 5, 10, 9, 2, 345000, '/storage/app/38', 5, '2017-04-24 09:40:37', '2017-04-24 06:40:37', 222);

-- --------------------------------------------------------

--
-- Структура таблицы `areas`
--

CREATE TABLE IF NOT EXISTS `areas` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `areas`
--

INSERT INTO `areas` (`id`, `name`) VALUES
(1, 'Запорожская'),
(2, 'Днепропетровская'),
(3, 'Киевская');

-- --------------------------------------------------------

--
-- Структура таблицы `brands`
--

CREATE TABLE IF NOT EXISTS `brands` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `brands`
--

INSERT INTO `brands` (`id`, `name`) VALUES
(1, 'Renault'),
(2, 'Opel');

-- --------------------------------------------------------

--
-- Структура таблицы `cars`
--

CREATE TABLE IF NOT EXISTS `cars` (
  `id` int(11) NOT NULL,
  `brand_id` int(10) NOT NULL,
  `model_id` int(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cars`
--

INSERT INTO `cars` (`id`, `brand_id`, `model_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 2, 5),
(6, 2, 6),
(7, 2, 7),
(8, 2, 8),
(9, 2, 9),
(10, 2, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `cities`
--

CREATE TABLE IF NOT EXISTS `cities` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `cities`
--

INSERT INTO `cities` (`id`, `name`) VALUES
(1, 'Запорожье'),
(2, 'Бердянск'),
(3, 'Токмак'),
(4, 'Мелитополь'),
(5, 'Днепр'),
(6, 'Каменское'),
(7, 'Кривой Рог'),
(8, 'Киев'),
(9, 'Бровары'),
(10, 'Вышгород');

-- --------------------------------------------------------

--
-- Структура таблицы `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Структура таблицы `models`
--

CREATE TABLE IF NOT EXISTS `models` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `models`
--

INSERT INTO `models` (`id`, `name`) VALUES
(1, 'Laguna'),
(2, 'Megane'),
(3, 'Clio'),
(4, 'Logan'),
(5, 'Kadett'),
(6, 'Omega'),
(7, 'Astra'),
(8, 'Vivaro'),
(9, 'Meriva'),
(10, 'Adam');

-- --------------------------------------------------------

--
-- Структура таблицы `password_resets`
--

CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('ol4ik55555@mail.ru', '44a3a3ca3b424121e33ca86a4ad2e2cb0b08238b1ec0d83bafc77cc490beae16', '2017-04-23 05:04:17');

-- --------------------------------------------------------

--
-- Структура таблицы `places`
--

CREATE TABLE IF NOT EXISTS `places` (
  `id` int(11) NOT NULL,
  `area_id` int(11) NOT NULL,
  `city_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `places`
--

INSERT INTO `places` (`id`, `area_id`, `city_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 1, 4),
(5, 2, 5),
(6, 2, 6),
(7, 2, 7),
(8, 3, 8),
(9, 3, 9),
(10, 3, 10);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Андрей Кузьменко', 'ol4ik55555@mail.ru', '$2y$10$OQqVUyX2dOBjm15s//6nL.gA.aJyGIqqExDD4PALDOuFvCrjtYgMG', 'pMctD6GYPIsoHRdoI47knsosm1kOjSLCdRLQLtJwzYWyurD4oRVJZq5nnG5k', '2017-04-18 12:18:23', '2017-04-22 07:20:39'),
(2, 'Ольга Кузьменко', 'kuzmenkophp@gmail.com', '$2y$10$rV1hbmYHIFi0b/EMqFpQ9u2FfFvBY57wgGkBJvtYmub9zx9jaMVAu', 'Bv5kNsnsOIJTc2kHM1I5nLOmZDM8HRadAqr9hQZab5MvFsn1BGHDmpwW1HIC', '2017-04-22 07:21:27', '2017-04-23 09:21:57'),
(3, 'Степан', 'kuzmenkophp@gmail.com11', '$2y$10$a6uF4NS3LTSWt0M2ATznWeBoWyMnQLUHFFBMX05rUDVoqlVfVjJny', 'YKiPXkVsgyI8A4Zbb3PoTWX8gyU03avBrD4f53KKO87j6c9qQ64bWz48CGKG', '2017-04-23 05:08:03', '2017-04-23 05:08:17'),
(4, 'Семён', 'Semen@mail.ru', '$2y$10$W51ZX8USkJa4509/.UidJ..wABT8iJNPIxAIMKfcAJKzHzH5bKB12', 'G8jtl4OQMBiNs0u8o0ksaDWUxibXBzpUd6yYeGlqcwJDh6BGP5Uxaq5bIpZv', '2017-04-24 05:57:30', '2017-04-24 06:19:00'),
(5, 'Влад', 'Vlad@mail.ru', '$2y$10$ICZjPtdWJT4LCqwIJbjhIuwac7vb1sG4vg2m/lRVT1Jve3UvGvvya', NULL, '2017-04-24 06:20:51', '2017-04-24 06:20:51');

-- --------------------------------------------------------

--
-- Структура таблицы `user_ads`
--

CREATE TABLE IF NOT EXISTS `user_ads` (
  `id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `ads_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user_ads`
--

INSERT INTO `user_ads` (`id`, `user_id`, `ads_id`) VALUES
(1, 1, 1);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ads`
--
ALTER TABLE `ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `places_id` (`places_id`),
  ADD KEY `cars_id` (`cars_id`);

--
-- Индексы таблицы `areas`
--
ALTER TABLE `areas`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `cars`
--
ALTER TABLE `cars`
  ADD PRIMARY KEY (`id`),
  ADD KEY `brand_id` (`brand_id`,`model_id`),
  ADD KEY `brand_id_2` (`brand_id`,`model_id`),
  ADD KEY `model_id` (`model_id`);

--
-- Индексы таблицы `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `models`
--
ALTER TABLE `models`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Индексы таблицы `places`
--
ALTER TABLE `places`
  ADD PRIMARY KEY (`id`),
  ADD KEY `area_id` (`area_id`,`city_id`),
  ADD KEY `city_id` (`city_id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Индексы таблицы `user_ads`
--
ALTER TABLE `user_ads`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`,`ads_id`),
  ADD KEY `ads_id` (`ads_id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ads`
--
ALTER TABLE `ads`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT для таблицы `areas`
--
ALTER TABLE `areas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `brands`
--
ALTER TABLE `brands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `cars`
--
ALTER TABLE `cars`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `models`
--
ALTER TABLE `models`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `places`
--
ALTER TABLE `places`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `ads`
--
ALTER TABLE `ads`
  ADD CONSTRAINT `ads_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `ads_ibfk_2` FOREIGN KEY (`places_id`) REFERENCES `places` (`id`),
  ADD CONSTRAINT `ads_ibfk_3` FOREIGN KEY (`cars_id`) REFERENCES `cars` (`id`);

--
-- Ограничения внешнего ключа таблицы `cars`
--
ALTER TABLE `cars`
  ADD CONSTRAINT `cars_ibfk_1` FOREIGN KEY (`brand_id`) REFERENCES `brands` (`id`),
  ADD CONSTRAINT `cars_ibfk_2` FOREIGN KEY (`model_id`) REFERENCES `models` (`id`);

--
-- Ограничения внешнего ключа таблицы `places`
--
ALTER TABLE `places`
  ADD CONSTRAINT `places_ibfk_1` FOREIGN KEY (`area_id`) REFERENCES `areas` (`id`),
  ADD CONSTRAINT `places_ibfk_2` FOREIGN KEY (`city_id`) REFERENCES `cities` (`id`);

--
-- Ограничения внешнего ключа таблицы `user_ads`
--
ALTER TABLE `user_ads`
  ADD CONSTRAINT `user_ads_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `user_ads_ibfk_2` FOREIGN KEY (`ads_id`) REFERENCES `ads` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
